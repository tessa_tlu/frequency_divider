`timescale 1ns / 1ps


module div
(   
    input wire clk_in,
    output reg clk_out

);

up_down_counter # (1, 14) count (.up_down(1'b1), .reset(1'b1), .clk(clk_in), .qout(clk_out))

endmodule



