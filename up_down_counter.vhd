`timescale 1ns / 1ps
module up_down_counter # (step = 1, mod = 16) (
    input up_down,
    input reset,
    input clk,
    output reg qout
);
reg [3:0] cnt;
always @ (clk)
    if (reset == 1)
        qout <= 0;
    else if (up_down == 0)
        begin
            if (cnt >= mod)
            begin
                cnt <= 4'b0000;
                qout <= ~qout;
            end
            else
                cnt <= cnt + 4'b0001;
        end
    else if (up_down == 1)
        qout <= (qout + mod - step) % mod;
    
endmodule
