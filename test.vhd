`timescale 1ns / 1ps

module test();

reg reset;
reg clk;

wire [3:0] qout;

up_down_counter #(1,14)
up_down_counter (.up_down(1'b0), .reset(reset), .clk(clk), .qout(qout));

initial begin 
#10
    reset = 1;
    clk = 0;
#10
    reset = 1;
#10
    reset = 0;
end

always #10 clk = ~clk;

endmodule

