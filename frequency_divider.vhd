`timescale 1ns / 1ps



module tb();

    reg clk;
    wire out;
    
    initial
    begin
        clk <= 0;
    end
    always #10 clk <= ~clk;
    
    div div
    (
        .clk_in(clk),
        .clk_out(out)
     );
endmodule

